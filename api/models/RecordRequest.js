/**
 * RecordRequest.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    requested_for: {
      model: 'User'
    },
    requested_by: {
      model: 'User'
    },
    status: {
      type: 'string',
      defaultsTo: 'pending',
      enum: ['pending', 'approved', 'denied']
    }

  }
};

