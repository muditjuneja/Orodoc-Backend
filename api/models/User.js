/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
const bcrypt = require('bcrypt');

module.exports = {

  attributes: {
    user_name: {
      type: 'string',
      unique: true
    },
    password: {
      type: 'string'
    },
    name: 'string',
    age: 'integer',
    email: 'string',
    role: {
      type: 'integer'
    }
  },

  beforeCreate: async function (user, next) {
    try {
      let hash = await bcrypt.hash(user.password, 10);
      user.password = hash;
      next();
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
};

