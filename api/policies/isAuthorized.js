/**
 * isAuthorized
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = async function (req, res, next) {
    let token;
    if (req.headers && req.headers.authorization) {
        let parts = req.headers.authorization.split(' ');
        if (parts.length == 2) {
            let scheme = parts[0],
                credentials = parts[1];

            if (/^Bearer$/i.test(scheme)) {
                token = credentials;
            }
        } else {
            return res.json(401, { message: 'Invalid token' });
        }
    } else if (req.param('token')) {
        token = req.param('token');
        // We delete the token from param to not mess with blueprints
        delete req.query.token;
    } else {
        return res.json(401, { message: 'No Authorization header was found' });
    }
    req.token = token;
    JWTService.verify(token, req._sails, function (err, payload) {
        if (err) return res.json(401, { message: 'Invalid token', 'valid': false });
        req.payload = payload; // This is the decrypted token or the payload you provided
        next();
    });
};