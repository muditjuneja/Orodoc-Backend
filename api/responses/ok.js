/**
 * 200 (OK) Response
 *
 * Usage:
 * return res.ok();
 * return res.ok(data);
 * return res.ok(data, 'auth/login');
 *
 * @param  {Object} data
 * @param  {String|Object} options
 *          - pass string to render specified view
 */

module.exports = function sendOK(data, options) {

  // Get access to `req`, `res`, & `sails`
  var req = this.req;
  var res = this.res;
  var sails = req._sails;
  let response = { data: data, valid: true };
  if (options) {
    if ('message' in options) {
      response.message = options.message;
    }

    if ('valid' in options) {
      response.valid = options.valid;
    }
  }
  return res.json(response);

};
