/**
 * jwToken
 *
 * @description :: JSON Webtoken Service for sails
 * @help        :: See https://github.com/auth0/node-jsonwebtoken & http://sailsjs.org/#!/documentation/concepts/Services
 */

const jwt = require('jsonwebtoken');
    

// Generates a token from supplied payload
module.exports.issue = function(payload, sails) {
    console.log(payload);
    return jwt.sign(
        payload.toJSON(),
        sails.config.jwtSecret, {
            expiresIn: 60 * 60 * 24 * 7 // Token Expire time
        }
    );
};

// Verifies token on a request
module.exports.verify = function(token, sails, callback) {
    return jwt.verify(
        token, // The token to be verified
        sails.config.jwtSecret, // Same token we used to sign
        {}, // No Option, for more see https://github.com/auth0/node-jsonwebtoken#jwtverifytoken-secretorpublickey-options-callback
        callback //Pass errors or decoded token to callback
    );
};