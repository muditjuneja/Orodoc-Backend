// Service for User DB calls

const bcrypt = require('bcrypt');
const ObjectID = require('mongodb').ObjectID;

function create(data) {
    return User.create(data);
}


function get(user_name, email) {
    return User.find({ 'or': [{ user_name: user_name }, { email: email }] });
}

function getOne(user_name) {
    return User.findOne({ user_name: user_name });
}


function authenticate(password, stored_password) {
    return bcrypt.compare(password, stored_password);
}


function requestRecord(data) {
    return RecordRequest.create(data);
}

function getAllRequests(user_id, type, status) {
    if (!status) {
        status = 'pending';
    }
    let filter = { status: status };
    //Get the requests for patients
    if (type == 1) {
        filter.requested_for = user_id;
        return RecordRequest.find(filter).populate('requested_by');
    } else { // Get the requests for pharmacist and doctors
        filter.requested_by = user_id;
        return RecordRequest.find(filter).populate('requested_for');
    }

}


function updateRecordRequest(_id, status) {
    // console.log(new ObjectID(_id));
    return RecordRequest.update({ id: _id }, { status: status });
}


function getUsers(role) {
    return User.find({ role: role });
}

module.exports = {
    create,
    get,
    getOne,
    authenticate,
    requestRecord,
    getAllRequests,
    updateRecordRequest,
    getUsers
}