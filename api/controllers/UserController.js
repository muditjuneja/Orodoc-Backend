/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * {"user_name":"Mudit","password":"hey","role":1}
   */
  create: async (req, res) => {
    const data = req.body;
    try {
      let users_in_db = await UserService.get(data.user_name, data.email);
    //   console.log(users_in_db);
      // let user_in_db = false;
      if (!users_in_db.length) {
        let user = await UserService.create(data);
        delete user.password;
        return res.ok({
          user: user,
          token: JWTService.issue(user, req._sails)
        });
      } else {
        return res.ok({}, {
          message: 'User with same username or email already exist.',
          valid: false
        });
      }

    } catch (error) {
      return res.badRequest(error);
    }
  },


  /**
   * {"user_name":"Mudit","password":"hey"}
   */
  login: async (req, res) => {
    const data = req.body;
    try {
      let user = await UserService.getOne(data.user_name);
      if (user) {
        let is_password_correct = await UserService.authenticate(data.password, user.password);
        if (is_password_correct) {
          delete user.password;
          return res.ok({
            user: user,
            token: JWTService.issue(user, req._sails)
          });
        } else {
          return res.ok({}, {
            message: 'Invalid Credentials.',
            valid: false
          });
        }
      } else {
        return res.ok({}, {
          message: 'No User found.',
          valid: false
        });
      }
    } catch (error) {
      return res.badRequest(error);
    }
  },

  /**
   * {"requested_for":"5a4e03065d0b5f1a73ab3dc6","requested_by":"5a4e023d3d0e4555728c8b9e"}
   * status = 'pending' (default)
   */
  requestRecord: async (req, res) => {
    const data = req.body;
    try {
      let request = await UserService.requestRecord(data);
      return res.ok(request);
    } catch (error) {
      return res.badRequest(error);
    }
  },

  getRequests: async (req, res) => {
    const type = req.params.type;
    const _id = req.params._id;
    const status = req.params.status;
    try {
      let records = await UserService.getAllRequests(_id, type, status);
      return res.ok(records);
    } catch (error) {
      return res.badRequest(error);
    }
  },

  updateRecordRequest: async (req, res) => {
    const _id = req.params._id;
    const status = req.body.status;
    try {
      console.log(_id, status);
      let response = await UserService.updateRecordRequest(_id, status);
      console.log(response);
      if (!response.length) {
        return res.ok({}, {
          message: 'No record found.',
          valid: false
        });
      }
      return res.ok(response[0]);
    } catch (error) {
      return res.badRequest(error);
    }
  },

  getUsers: async (req, res) => {
    const role = req.params.role;
    try {
      let users = await UserService.getUsers(role);
      return res.ok(users);
    } catch (error) {
      return res.badRequest(error);
    }
  }
};
